note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	NEW_TEST_SET


inherit
	EQA_TEST_SET

redefine on_prepare end

feature
	g : GAME

	on_prepare
	do
		create g.make
		assert("game test ",g /= VOID)
	end

end
